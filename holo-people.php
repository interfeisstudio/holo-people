<?php
/*
Plugin Name: Holo People
Plugin URI: http://www.interfeis.com/
Description: Holo People is a wordpress plugin for display some people in your team or organization.
Version: 1.1.1
Author: interfeis
Author URI: http://www.interfeis.com
License: GPL
*/

/*  Copyright 2016  Interfeis

    Holo People is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//to block direct access
if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

//global variable for this plugin
$pathinfo	= pathinfo(__FILE__);
$plugins_dir = dirname( __FILE__ );

include_once( $plugins_dir.'/widgets/holo-people-widget.php' );

class Holo_People{

	var $imagesizes;
	var	$langval;
	var	$version;
	var $defaultattr;
	var $postslug;
	var $taxonomslug;
	var $posttype;
	var $posttaxonomy;

	function __construct(){
		// Register the shortcode to the function ep_shortcode()
		add_shortcode( 'people', array($this, 'holo_people') );

        add_action('vc_before_init', array( $this, 'holo_vc_map'));

		// Register the options menu
		add_action('admin_init', 'flush_rewrite_rules');

		//Register the Portfolio Menu
		add_action('init', array($this, 'holo_pf_post_type'));
		add_action('init', array($this, 'holo_pf_action_init'));
		add_action('after_setup_theme', array($this, 'holo_pf_setup'));

		//Customize the Portfolio List in the wp-admin
		add_filter('manage_edit-peoplepost_columns', array($this, 'holo_pf_add_list_columns'));
		add_action('manage_peoplepost_posts_custom_column', array($this, 'holo_pf_manage_column'));
		add_action( 'restrict_manage_posts', array($this, 'holo_pf_add_taxonomy_filter') );

		$this->version		= $this->holo_plugin_version();
		$this->postslug		= $this->holo_postslug();
		$this->taxonomslug	= $this->holo_taxonomslug();
		$this->posttype		= $this->holo_posttype();
		$this->posttaxonomy	= $this->holo_taxonomy();
	}

	//Get the version of portfolio
	function holo_plugin_version(){
		$this->version = "1.0";

		return $this->version;
	}

	function holo_lang(){
		$thelang = 'holo';
		return $thelang;
	}

	function holo_shortname(){
		$theshortname = 'holo';
		return $theshortname;
	}

	function holo_initial(){
		$theinitial = 'nvr';
		return $theinitial;
	}

	function holo_posttype(){
		$this->posttype = 'peoplepost';
		return $this->posttype;
	}

	function holo_taxonomy(){
		$this->posttaxonomy = 'peoplecat';
		return $this->posttaxonomy;
	}

	function holo_postslug(){
		$this->postslug = 'people';
		return $this->postslug;
	}

	function holo_taxonomslug(){
		$this->taxonomslug = 'peoplecat';
		return $this->taxonomslug;
	}

	function holo_pf_md5hash($str = ''){
		return md5($str);
	}

	//Get the image size for every column
	function holo_pf_setsize(){

		//set image size for every column in here.
		$this->imagesizes = array(
			array(
				"num"		=> 'default',
				"namesize"	=> 'people-image',
				"width" 	=> 700,
				"height" 	=> 700
			)

		);
		return $this->imagesizes;
	}

	function holo_pf_setup(){
		add_theme_support( 'post-thumbnails' );
		$imagesizes = $this->holo_pf_setsize();
		foreach($imagesizes as $imgsize){
			add_image_size( $imgsize["namesize"], $imgsize["width"], $imgsize["height"], true ); // Portfolio Thumbnail
		}
	}

	function holo_pf_getthumbinfo($col){
		$imagesizes = $this->holo_pf_setsize();
		foreach($imagesizes as $imgsize){
			if($col==$imgsize["num"]){
				return $imgsize;
			}
		}
		return false;
	}

	function holo_people($atts, $content = null) {
		extract(shortcode_atts(array(
			'id' 	=> '',
			'class'	=> '',
			'col' => '3',
			'cat' => '',
			'showposts' => 3,
			'showtitle' => 'yes',
			'showinfo' => 'yes',
			'showthumb' => 'yes'
		), $atts));

		$nvr_initial = $this->holo_initial();
		$nvr_shortname = $this->holo_shortname();

		$catname = get_term_by('slug', $cat, $this->holo_taxonomy());
		$showtitle = ($showtitle=='yes')? true : false;
		$showinfo = ($showinfo=='yes')? true : false;
		$showthumb = ($showthumb=='yes')? true : false;
		$showposts = (is_numeric($showposts))? $showposts : 5;

		if($col!='3' && $col!='4'){
			$col = '3';
		}

		if($col=='3'){
			$col = 3;
		}elseif($col=='4'){
			$col = 4;
		}else{
			$col = 3;
		}

		$imgsize = 'people-image';

		$qryargs = array(
			'post_type' => $this->holo_posttype(),
			'showposts' => $showposts
		);
		if($catname!=false){
			$qryargs['tax_query'] = array(
				array(
					'taxonomy' => $this->holo_taxonomy(),
					'field' => 'slug',
					'terms' => $catname->slug
				)
			);
		}

		$nvr_peopleqry = new WP_Query( $qryargs );

		$nvr_output = "";
		if( $nvr_peopleqry->have_posts() ){
			$nvr_output .= '<div class="nvr-people '.esc_attr( $class ).'">';
			$nvr_output .= '<ul class="row">';
			$i = 1;
			while ( $nvr_peopleqry->have_posts() ) : $nvr_peopleqry->the_post();

				if($col==3){
					$liclass = 'four columns';
				}elseif($col==4){
					$liclass = 'three columns';
				}else{
					$liclass = '';
				}

				$peopleid 		= get_the_ID();
				$custom 		= get_post_custom( $peopleid );
				$peopleinfo 	= (isset($custom['_'.$nvr_initial.'_people_info'][0]))? $custom['_'.$nvr_initial.'_people_info'][0] : "";
				$peoplethumb 	= (isset($custom['_'.$nvr_initial.'_people_thumb'][0]))? $custom['_'.$nvr_initial.'_people_thumb'][0] : "";
				$peoplepinterest= (isset($custom['_'.$nvr_initial.'_people_pinterest'][0]))? $custom['_'.$nvr_initial.'_people_pinterest'][0] : "";
				$peoplefacebook	= (isset($custom['_'.$nvr_initial.'_people_facebook'][0]))? $custom['_'.$nvr_initial.'_people_facebook'][0] : "";
				$peopletwitter 	= (isset($custom['_'.$nvr_initial.'_people_twitter'][0]))? $custom['_'.$nvr_initial.'_people_twitter'][0] : "";

				if($i%$col==1){
					$liclass .= ' alpha';
				}elseif($i%$col==0 && $col>1){
					$liclass .= ' last';
				}

				$nvr_output .= '<li class="'.esc_attr( $liclass ).'">';
					$nvr_output .='<div class="peoplecontainer">';
						if($showthumb){
							$nvr_output .='<div class="imgcontainer">';
								if($peoplethumb){
									$nvr_output .= '<img src="'.esc_url( $peoplethumb ).'" alt="'.esc_attr( get_the_title( $peopleid ) ).'" title="'. esc_attr( get_the_title( $peopleid ) ) .'" class="scale-with-grid" />';
								}elseif( has_post_thumbnail( $peopleid ) ){
									$nvr_output .= get_the_post_thumbnail( $peopleid, $imgsize, array('class' => 'scale-with-grid'));
								}else{
									$nvr_output .= '<img src="'.esc_url( plugin_dir_url( __FILE__ ).'images/testi-user.png' ).'" alt="'.esc_attr( get_the_title( $peopleid ) ).'" title="'. esc_attr( get_the_title( $peopleid ) ) .'" class="scale-with-grid" />';
								}
								$nvr_output .= '<div class="peoplecontent">';
									$nvr_output .= '<h5 class="peopleabout">'.__('About Me', 'holo-people').'</h5>';
									$nvr_output .= get_the_content();
								$nvr_output .= '</div>';
								$nvr_output .= '<div class="clearfix"></div>';
							$nvr_output .='</div>';

							$bqclass="";
						}else{
							$bqclass="nomargin";
						}

						$nvr_output .= '<div class="peopletitle '.esc_attr( $bqclass ).'">';
							if($showtitle || $showinfo){
								if($showtitle){
									$nvr_output .= '<h5 class="fontbold marginoff">'.get_the_title( $peopleid ).'</h5>';
								}
								if($showinfo){
									$nvr_output .= '<div class="peopleinfo">'.$peopleinfo.'</div>';
								}
							}
							$nvr_output .= '<div class="clearfix"></div>';
						$nvr_output .= '</div>';
						$nvr_output .= '<div class="peoplesocial">';
							if($peoplefacebook){
								$nvr_output .= '<a href="'.esc_url( $peoplefacebook ).'" target="_blank" class="fa fa-facebook"></a>';
							}
							if($peopletwitter){
								$nvr_output .= '<a href="'.esc_url( $peopletwitter ).'" target="_blank" class="fa fa-twitter"></a>';
							}
							if($peoplepinterest){
								$nvr_output .= '<a href="'.esc_url( $peoplepinterest ).'" target="_blank" class="fa fa-pinterest"></a>';
							}
							$nvr_output .= '<div class="clearfix"></div>';
						$nvr_output .= '</div>';

						$nvr_output .= '<div class="clearfix"></div>';
					$nvr_output .= '</div>';
				$nvr_output .= '</li>';

				$i++;
			endwhile;
				$nvr_output .= '<li class="clearfix"></li>';
			$nvr_output .= '</ul>';
			$nvr_output .= '<div class="clearfix"></div>';
			$nvr_output .= "</div>";
		}else{
			$nvr_output .= '<!-- no people post -->';
		}
		wp_reset_postdata();

		return do_shortcode($nvr_output);
	}

	/* Make a Portfolio Post Type */
	function holo_pf_post_type() {
		$posttype = $this->holo_posttype();
		$taxonom = $this->holo_taxonomy();
		$postslug = $this->holo_postslug();
		$taxonomslug = $this->holo_taxonomslug();

		register_post_type( $posttype,
					array(
					'label' => __('People', 'holo-people' ),
					'public' => true,
					'show_ui' => true,
					'show_in_nav_menus' => true,
					'rewrite' => array( 'slug' => $postslug, 'with_front' => false ),
					'hierarchical' => true,
					'menu_position' => 5,
					'has_archive' => true,
					'supports' => array(
										 'title',
										 'editor',
										 'thumbnail',
										 'excerpt',
										 'custom-fields',
										 'revisions')
						)
					);
		register_taxonomy($taxonom, $posttype, array(
			'hierarchical' => true,
			'label' =>  __('People Categories', 'holo-people'),
			'query_var' => true,
			'rewrite' => array( 'slug' => $taxonomslug, 'with_front' => false ),
			'show_ui' => true,
			'singular_name' => 'Category'
			));
	}

	function holo_pf_add_list_columns($portfolio_columns){

		$thetaxonomy = $this->holo_taxonomy();
		$new_columns = array();
		$new_columns['cb'] = '<input type="checkbox" />';

		$new_columns['title'] = __('People Name', 'holo-people');
		$new_columns['images'] = __('Images', 'holo-people');
		$new_columns['author'] = __('Author', 'holo-people');

		$new_columns[$thetaxonomy] = __('Categories', 'holo-people');

		$new_columns['date'] = __('Date', 'holo-people');

		return $new_columns;
	}

	function holo_pf_manage_column($column_name){
		global $post;
		$posttype = $this->holo_posttype();
		$taxonom = $this->holo_taxonomy();

		$id = $post->ID;
		$title = $post->post_title;
		switch($column_name){
			case 'images':
				$thumbnailid = get_post_thumbnail_id($id);
				$imagesrc = wp_get_attachment_image_src($thumbnailid, 'thumbnail');
				if($imagesrc){
					echo '<img src="'.$imagesrc[0].'" width="50" alt="'.$title.'" />';
				}else{
					_e('No Featured Image', 'holo-people');
				}
				break;

			case $taxonom:
				$postterms = get_the_terms($id, $taxonom);
				if($postterms){
					$termlists = array();
					foreach($postterms as $postterm){
						$termlists[] = '<a href="'.admin_url('edit.php?'.$taxonom.'='.$postterm->slug.'&post_type='.$posttype).'">'.$postterm->name.'</a>';
					}
					if(count($termlists)>0){
						$termtext = implode(", ",$termlists);
						echo $termtext;
					}
				}

				break;
		}
	}

	/* Filter Custom Post Type Categories */
	function holo_pf_add_taxonomy_filter() {
		global $typenow;
		$posttype = $this->holo_posttype();
		$taxonomy = $this->holo_taxonomy();
		if( $typenow==$posttype){
			$filters = array($taxonomy);
			foreach ($filters as $tax_slug) {
				$tax_obj = get_taxonomy($tax_slug);
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms($tax_slug);
				echo '<select name="'. esc_attr( $tax_slug ).'" id="'. esc_attr( $tax_slug ) .'" class="postform">';
				echo "<option value=''>".__('View All','holo-people')." "."$tax_name</option>";
				foreach ($terms as $term) {
					$selectedstr = '';
					if(isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug){
						$selectedstr = ' selected="selected"';
					}
					echo '<option value='. $term->slug. $selectedstr . '>' . $term->name .' (' . $term->count .')</option>';
				}
				echo "</select>";
			}
		}
	}

	function holo_pf_action_init(){
		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages

		$version = $this->holo_plugin_version();

		//Register and use this plugin main CSS
		wp_register_style('holo_skeleton', plugin_dir_url( __FILE__ ).'css/1140.css', 'normalize', '', 'screen, all');
		wp_enqueue_style('holo_skeleton');

		wp_register_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', 'normalize', '', 'screen, all');
		wp_enqueue_style('font-awesome-css');

		wp_register_style('holo_custom-people', plugin_dir_url( __FILE__ ).'css/holopeople.css', '', '', 'screen, all');
		wp_enqueue_style('holo_custom-people');
	}

	// The excerpt based on character
	function holo_pf_limit_char($excerpt, $substr=0, $strmore = "..."){
		$string = strip_tags(str_replace('...', '...', $excerpt));
		if ($substr>0) {
			$string = substr($string, 0, $substr);
		}
		if(strlen($excerpt)>=$substr){
			$string .= $strmore;
		}
		return $string;
	}

    function holo_vc_map(){
        if(function_exists('vc_map')){

            vc_map(
                array(
                    "name" => __( "People", 'holo-people'),
                    "base" => "people",
                    "class" => "",
                    "category" => __( "Holo", 'holo-people'),
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Custom Id", 'holo-people' ),
                            "param_name" => "id",
                            "value" => "",
                            "description" => __( "Input your custom id. (optional)", 'holo-people' )
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Custom Class", 'holo-people' ),
                            "param_name" => "class",
                            "value" => "",
                            "description" => __( "Input your custom class. (optional)", 'holo-people' )
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "People Category Slug", 'holo-people' ),
                            "param_name" => "cat",
                            "admin_label" => true,
                            "value" => "",
                            "description" => __( "Input the people category slugs.", 'holo-people' )
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __( "Columns", 'holo-people' ),
                            "param_name" => "col",
                            "admin_label" => true,
                            "value" => array(
                                __('3 Columns', 'holo-people') => "3",
                                __('4 Columns', 'holo-people') => "4"
                            ),
                            "description" => __( "Select the column of your brand.", 'holo-people' )
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __( "Show Posts", 'holo-people' ),
                            "param_name" => "showposts",
                            "value" => '-1',
                            "description" => __( "Input the number of brand that you want to display.", 'holo-people' )
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __( "Show Title", 'holo-people' ),
                            "param_name" => "showtitle",
                            "admin_label" => true,
                            "value" => array(
                                __('Yes', 'holo-people') => "yes",
                                __('No', 'holo-people') => "no"
                            ),
                            "description" => __( "Select 'No' if you dont want to show the title.", 'holo-people' )
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __( "Show Info", 'holo-people' ),
                            "param_name" => "showinfo",
                            "admin_label" => true,
                            "value" => array(
                                __('Yes', 'holo-people') => "yes",
                                __('No', 'holo-people') => "no"
                            ),
                            "description" => __( "Select 'No' if you dont want to show the info.", 'holo-people' )
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __( "Show Thumbnail", 'holo-people' ),
                            "param_name" => "showthumb",
                            "admin_label" => true,
                            "value" => array(
                                __('Yes', 'holo-people') => "yes",
                                __('No', 'holo-people') => "no"
                            ),
                            "description" => __( "Select 'No' if you dont want to show the thumbnail.", 'holo-people' )
                        )
                    )
                )
            );
        }
    }

}

$thepeople = new Holo_People();

add_action("widgets_init", "holo_people_load_widgets");
function holo_people_load_widgets() {
	register_widget("Holo_PeopleWidget");
}
?>
