<?php
// =============================== Holo People Widget ======================================
class Holo_PeopleWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_holo_people', 'description' => esc_html__('Holo - People', "holo-people") );
		parent::__construct('holo-people-widget', esc_html__('Holo - People',"holo-people"), $widget_ops);
	}
	
	function widget( $args, $instance ) {
		global $wpdb, $comments, $comment;

		extract($args, EXTR_SKIP);
        $id      = apply_filters('widget_holo_people_id', empty($instance['id']) ? '' : $instance['id']);
		$class      = apply_filters('widget_holo_people_class', empty($instance['class']) ? '' : $instance['class']);
        $cat        = apply_filters('widget_holo_people_cat', empty($instance['cat']) ? '' : $instance['cat']);
        $col        = apply_filters('widget_holo_people_col', empty($instance['col']) ? '' : $instance['col']);
        $showposts  = apply_filters('widget_holo_people_showposts', empty($instance['showposts']) ? '' : $instance['showposts']);
        $showinfo   = apply_filters('widget_holo_people_showinfo', empty($instance['showinfo']) ? '' : $instance['showinfo']);
		$showtitle  = apply_filters('widget_holo_people_showtitle', empty($instance['showtitle']) ? '' : $instance['showtitle']);
        $showthumb  = apply_filters('widget_holo_people_showthumb', empty($instance['showthumb']) ? '' : $instance['showthumb']);
        
        $scparams = '';
        if(trim($id)!=''){
            $scparams .= ' id="'.esc_attr($id).'"';
        }
        
        if(trim($class)!=''){
            $scparams .= ' class="'.esc_attr($class).'"';
        }
        
        if(trim($cat)!=''){
            $scparams .= ' cat="'.esc_attr($cat).'"';
        }
        
        if(trim($col)!=''){
            $scparams .= ' col="'.esc_attr($col).'"';
        }
        
        if(trim($showposts)!=''){
            $scparams .= ' showposts="'.esc_attr($showposts).'"';
        }
        
        if(trim($showinfo)!=''){
            $scparams .= ' showinfo="'.esc_attr($showinfo).'"';
        }
        
        if(trim($showtitle)!=''){
            $scparams .= ' showtitle="'.esc_attr($showtitle).'"';
        }
        
        if(trim($showthumb)!=''){
            $scparams .= ' showthumb="'.esc_attr($showthumb).'"';
        }
        
        echo do_shortcode('[people '.$scparams.']');
	}
	
	function update($new_instance, $old_instance) {				
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
		$instance['id'] = (isset($instance['id']))? $instance['id'] : "";
        $instance['class'] = (isset($instance['class']))? $instance['class'] : "";
		$instance['cat'] = (isset($instance['cat']))? $instance['cat'] : "";
        $instance['col'] = (isset($instance['col']))? $instance['col'] : "";
        $instance['showposts'] = (isset($instance['showposts']))? $instance['showposts'] : "";
        $instance['showtitle'] = (isset($instance['showtitle']))? $instance['showtitle'] : "";
        $instance['showinfo'] = (isset($instance['showinfo']))? $instance['showinfo'] : "";
        $instance['showthumb'] = (isset($instance['showthumb']))? $instance['showthumb'] : "";
		
        $cols = array(
            '1' => __('1 Column', 'holo-people'),
            '2' => __('2 Columns', 'holo-people'),
            '3' => __('3 Columns', 'holo-people')
        );
        
        $arrsval = array(
            'yes' => __('Yes', 'holo-people'),
            'no' => __('No', 'holo-people')
        );
        
        $id = esc_attr($instance['id']);
        $class = esc_attr($instance['class']);
		$cat = esc_attr($instance['cat']);
        $col = esc_attr($instance['col']);
		$showposts = esc_attr($instance['showposts']);
        $showtitle = esc_attr($instance['showtitle']);
        $showinfo  = esc_attr($instance['showinfo']);
        $showthumb = esc_attr($instance['showthumb']);
        

        ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id('id') ); ?>"><?php esc_html_e('Custom ID:', "holo-people"); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('id') ); ?>" name="<?php echo esc_attr( $this->get_field_name('id') ); ?>" type="text" value="<?php echo esc_attr( $id ); ?>" /></label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('class') ); ?>"><?php esc_html_e('Custom Class:', "holo-people"); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('class') ); ?>" name="<?php echo esc_attr( $this->get_field_name('class') ); ?>" type="text" value="<?php echo esc_attr( $class ); ?>" /></label></p>
			
            <p><label for="<?php echo esc_attr( $this->get_field_id('cat') ); ?>"><?php esc_html_e('Brand Category Slug:', "holo-people" ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('cat') ); ?>" name="<?php echo esc_attr( $this->get_field_name('cat') ); ?>" type="text" value="<?php echo esc_attr( $cat ); ?>" /></label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('col') ); ?>"><?php esc_html_e('Number of Columns:', "holo-people" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('col') ); ?>" name="<?php echo esc_attr( $this->get_field_name('col') ); ?>">
                    <?php foreach($cols as $colval => $colname){ ?>
                        <?php $selected = ($colval==$col)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $colval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $colname ); ?></option>
                    <?php }?>
                </select>
            </label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>"><?php esc_html_e('Showposts:', "holo-people" ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showposts') ); ?>" type="text" value="<?php echo esc_attr( $showposts ); ?>" /></label></p>
            
            <p><label for="<?php echo esc_attr( $this->get_field_id('showtitle') ); ?>"><?php esc_html_e('Show Title:', "holo-people" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('showtitle') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showtitle') ); ?>">
                    <?php foreach($arrsval as $arrval => $arrname){ ?>
                        <?php $selected = ($arrval==$showtitle)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $arrval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $arrname ); ?></option>
                    <?php }?>
                </select>
            </label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('showinfo') ); ?>"><?php esc_html_e('Show Info:', "holo-people" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('showinfo') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showinfo') ); ?>">
                    <?php foreach($arrsval as $arrval => $arrname){ ?>
                        <?php $selected = ($arrval==$showinfo)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $arrval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $arrname ); ?></option>
                    <?php }?>
                </select>
            </label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('showthumb') ); ?>"><?php esc_html_e('Show Thumbnail:', "holo-people" ); ?> 
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('showthumb') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showthumb') ); ?>">
                    <?php foreach($arrsval as $arrval => $arrname){ ?>
                        <?php $selected = ($arrval==$showthumb)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $arrval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $arrname ); ?></option>
                    <?php }?>
                </select>
            </label></p>
        <?php 
    }
}
?>